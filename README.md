# vue-example

> vue.js示例项目，实现了一个简单的框架

## 快速开始

``` bash
# 安装项目依赖类库
npm install
#如果执行过程卡住，可以执行多次，或者用cnpm命令替代
npm install -g cnpm --registry=https://registry.npm.taobao.org;cnpm install


# 启动localhost:8080 服务，进行开发测试
npm run dev

# 编译项目
npm run build

# 编译项目并查看日志
npm run build --report

# 执行单元测试
npm run unit

# 执行 e2e 测试
npm run e2e

# 执行所有测试
npm test
```



相关更多详细说明，请查看[手册](http://vuejs-templates.github.io/webpack/)和[vue-loader的文档](http://vuejs.github.io/vue-loader)。
